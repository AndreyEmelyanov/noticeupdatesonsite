import javax.sound.sampled.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        try
        {
            while (true)
            {
                HttpURLConnection con = (HttpURLConnection) new URL("https://vk.com/wasabisamara").openConnection(); // url to connection
                InputStream in = new BufferedInputStream(con.getInputStream());
                Scanner sc = new Scanner(in);
                StringBuffer sb = new StringBuffer();
                while (sc.hasNextLine())
                {
                    sb.append(sc.nextLine() + "\n");
                }
                String exit = sb.toString();

                if (exit.indexOf("<a class=\"link_header\" href=\"/wasabisamara?own=1#wall\"><h4 class=\"slim_header clearfix\"><span class=\"slim_header_label\">1<span class=\"num_delim\"> </span>969 записей</span><span class=\"slim_header_rl\">к записям сообщества</span></h4></a>")> 0) // variable part of the website
                {
                    con.disconnect();
                    System.out.println("step");
                    Thread.sleep(10000);
                }
                else
                {
                    File soundFile = new File("D:/Учеба/test/src/1.wav"); //way to audio file

                    AudioInputStream ais = AudioSystem.getAudioInputStream(soundFile);

                    Clip clip = AudioSystem.getClip();

                    clip.open(ais);

                    clip.setFramePosition(0);
                    clip.start();

                    Thread.sleep(clip.getMicrosecondLength() / 1000); // pause before closing
                    clip.stop();
                    clip.close();
                    System.out.println("found changes");
                    System.exit(0);
                }
            }
        }
        catch (Exception e)
        {
            File soundFile = new File("D:/Учеба/test/src/1.wav");

            AudioInputStream ais = null;
            try
            {
                ais = AudioSystem.getAudioInputStream(soundFile);
            }
            catch (UnsupportedAudioFileException e1)
            {
                e1.printStackTrace();
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }

            Clip clip = null;
            try
            {
                clip = AudioSystem.getClip();
            }
            catch (LineUnavailableException e1)
            {
                e1.printStackTrace();
            }

            try
            {
                clip.open(ais);
            }
            catch (LineUnavailableException e1)
            {
                e1.printStackTrace();
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }

            clip.setFramePosition(0);
            clip.start();
        }
    }
}
